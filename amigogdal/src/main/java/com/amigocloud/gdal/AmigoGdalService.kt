/*
 *  AmigoCloud
 *
 *  Copyright (c) 2019 AmigoCloud Inc., All rights reserved.
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the Software,
 *  and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 *  OR OTHER DEALINGS IN THE SOFTWARE.
*/

package com.amigocloud.gdal

import android.app.Service
import android.content.Intent
import android.os.AsyncTask
import android.os.IBinder

class AmigoGdalService : Service() {

    var callback: IAmigoGdalServiceCallback?=null

    override fun onBind(intent: Intent?): IBinder? = binder

    private val binder = object : IAmigoGdalService.Stub() {

        override fun registerCallback(cb: IAmigoGdalServiceCallback?) {
            callback = cb
            AmigoGdalJNI.callback = cb
        }

        override fun unregisterCallback(cb: IAmigoGdalServiceCallback?) { callback = null }

        override fun ogr2ogr(argv: List<String>) {
            GdalAsyncTask(argv, callback).execute()
        }
    }

    internal class GdalAsyncTask(private val argv: List<String>, private val callback: IAmigoGdalServiceCallback?) :
            AsyncTask<Void, Void, Int>() {

        override fun doInBackground(vararg voids: Void): Int {
            return AmigoGdalJNI.ogr2ogr(argv.toTypedArray())
        }

        override fun onPostExecute(result: Int) {
            callback?.onDone(result)
        }
    }

}
