/*
 *  AmigoCloud
 *
 *  Copyright (c) 2019 AmigoCloud Inc., All rights reserved.
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the Software,
 *  and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 *  OR OTHER DEALINGS IN THE SOFTWARE.
*/


#include <jni.h>
#include <android/log.h>
#include <string>
#include <vector>
#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

extern int ogr2ogr(std::vector<std::string> &vsArgv);

extern "C"
{

static int pfd[2];
static pthread_t thr;
static const char *tag = "amigogdal";
#define BUFFER_SIZE 4096
JavaVM *gJavaVM;
jobject gInterfaceObject;


// Calling the callback Java method from C
#define CALLBACK_INIT \
	int status; \
	JNIEnv *env; \
	bool isAttached = false; \
	status = gJavaVM->GetEnv((void **) &env, JNI_VERSION_1_6); \
	if(status < 0) \
	{ \
		status = gJavaVM->AttachCurrentThread(&env, NULL); \
		if(status < 0) \
		{ \
			__android_log_write(ANDROID_LOG_INFO, tag, "CALLBACK_INIT: failed to attach current thread"); \
			return; \
		} \
		isAttached = true; \
	} \
	jclass interfaceClass = env->GetObjectClass(gInterfaceObject); \
	if(!interfaceClass) \
	{ \
		__android_log_write(ANDROID_LOG_INFO, tag, "CALLBACK_INIT: failed to get class reference"); \
		if(isAttached) gJavaVM->DetachCurrentThread(); \
		return; \
	}

static void console_callback_handler(const char *s)
{
    CALLBACK_INIT
    /* Find the callBack method ID */
    jmethodID method = env->GetStaticMethodID(interfaceClass, "consoleCallBack", "(Ljava/lang/String;)V");
    if(!method)
    {
        __android_log_write(ANDROID_LOG_INFO, tag, "callback_handler: failed to get method ID");
        if(isAttached) gJavaVM->DetachCurrentThread();
        return;
    }
    /* Construct a Java string */
    jstring js = env->NewStringUTF(s);
    env->CallStaticVoidMethod(interfaceClass, method, js);
    env->DeleteLocalRef(js);
    env->DeleteLocalRef(interfaceClass);
    if(isAttached) gJavaVM->DetachCurrentThread();
}

static void *loggin_thread_func(void *) {
    ssize_t rdsz;
    char buf[BUFFER_SIZE];
    while ((rdsz = read(pfd[0], buf, sizeof buf - 1)) > 0) {
        if(buf[rdsz - 1] == '\n') --rdsz;
        buf[rdsz] = 0;  /* add null-terminator */
        __android_log_write(ANDROID_LOG_INFO, tag, buf);
        console_callback_handler(buf);
    }
    return nullptr;
}

int start_logger(const char *app_name) {
    tag = app_name;

    /* make stdout line-buffered and stderr unbuffered */
    setvbuf(stdout, 0, _IOLBF, 0);
    setvbuf(stderr, 0, _IONBF, 0);

    /* create the pipe and redirect stdout and stderr */
    pipe(pfd);
    dup2(pfd[1], 1);
    dup2(pfd[1], 2);

    /* spawn the logging thread */
    if (pthread_create(&thr, 0, loggin_thread_func, 0) == -1)
        return -1;
    pthread_detach(thr);
    return 0;
}

JNIEXPORT jint JNICALL
Java_com_amigocloud_gdal_AmigoGdalJNI_ogr2ogr(JNIEnv *env, jobject obj, jobjectArray argv);

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    __android_log_print(ANDROID_LOG_INFO, __FUNCTION__, "AmigoGDAL starting");
    JNIEnv *env;
    jclass cls;
    jmethodID constr;

    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }
    gJavaVM = vm;
    if (vm->GetEnv((void **)&env, JNI_VERSION_1_6) != JNI_OK)
        return -1;
    cls = env->FindClass("com/amigocloud/gdal/AmigoGdalJNI");
    constr = env->GetMethodID(cls, "<init>", "()V");
    gInterfaceObject = env->NewGlobalRef(env->NewObject(cls, constr));

    start_logger("AmigoGDAL");
    return JNI_VERSION_1_6;
}

JNIEXPORT jint JNICALL
Java_com_amigocloud_gdal_AmigoGdalJNI_ogr2ogr(JNIEnv *env, jobject obj, jobjectArray argv) {
    std::vector<std::string> params;
    int stringCount = env->GetArrayLength(argv);
    params.emplace_back("ogr2ogr");
    for (int i = 0; i < stringCount; i++) {
        auto string = (jstring) (env->GetObjectArrayElement(argv, i));
        params.emplace_back(env->GetStringUTFChars(string, 0));
    }
    return ogr2ogr(params);
}

}