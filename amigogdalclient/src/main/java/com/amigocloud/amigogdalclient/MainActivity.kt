/*
 *  AmigoCloud
 *
 *  Copyright (c) 2019 AmigoCloud Inc., All rights reserved.
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the Software,
 *  and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 *  OR OTHER DEALINGS IN THE SOFTWARE.
*/

package com.amigocloud.amigogdalclient

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.IBinder
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.amigocloud.gdal.IAmigoGdalService
import com.amigocloud.gdal.IAmigoGdalServiceCallback

class MainActivity : AppCompatActivity() {

    val TAG = "AmigoGdalClient"
    var iService: IAmigoGdalService? = null

    var callback: IAmigoGdalServiceCallback = object: IAmigoGdalServiceCallback.Stub() {
        override fun onDone(status: Int) {
            Log.d(TAG, "onDone($status)")
        }
        override fun onConsoleOut(line: String) {
            Log.d("ogr2ogr", line)
        }
    }

    val mConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // Following the example above for an AIDL interface,
            // this gets an instance of the IRemoteInterface, which we can use to call on the service
            Log.d(TAG, "Service Connected")
            iService = IAmigoGdalService.Stub.asInterface(service)
            iService?.registerCallback(callback)
            val path = Environment.getExternalStorageDirectory().getPath()
//            iService?.ogr2ogr(listOf("-f", "ESRI Shapefile", "$path/Downloads/primary.shp", "$path/Downloads/primary.geojson"))
            iService?.ogr2ogr(listOf("--formats"))
        }

        override fun onServiceDisconnected(className: ComponentName) {
            Log.e(TAG, "Service has unexpectedly disconnected")
            iService = null
        }
    }

    fun bind() {
        val intent = Intent()
        intent.component = ComponentName("com.amigocloud.gdal", "com.amigocloud.gdal.AmigoGdalService")
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(isStoragePermissionGranted())
            bind()
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Write permission is granted")
                return true
            } else {
                Log.v(TAG, "Write permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE), 2)
                return false
            }
        } else {
            Log.v(TAG, "Write permission is granted")
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            when (requestCode) {
                2 -> {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.v(TAG, "Permission: ${permissions[0]} was granted ")
                        bind()
                    }
                }
            }
        }
    }
}
